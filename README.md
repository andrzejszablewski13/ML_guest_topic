ML_guest_topic

Projekt zaliczeniowy z uczenia maszynowego w pythonie

Polega na zgadnięcie tematu/kategorii w której napisany został komentarz

żródło bazy danych : https://www.kaggle.com/aashita/nyt-comments?select=CommentsApril2017.csv

pliki:

    -MLShortVersion.ipynb : skrócona wersja kodu, czytelniejszy i z możliwością przetestowania używając własnego tekstu ale niepełny

    -MainML.ipynb : pełny kod z wszystkimi elementami ale bez interakcji z użytkownikiem i nieco chaotyczny

    -environment.yml : środowisko conda do kodu na windowsie

    -my_model6012 : stworzony model oparty na logistic regression, accuracy wynosi ok. 51% 
    
    -archive : pobrana baza danych i poseleckonowana na artykuły i komentarze
